import React, { Component } from 'react';
import { StyleSheet, View, ImageBackground, TextInput, Text, Image, TouchableOpacity, Animated, 
    Dimensions, Keyboard, Platform
} from 'react-native';

import { StackNavigator } from 'react-navigation';

import * as Animatable from 'react-native-animatable';
import { Icon } from 'native-base';

const SCREEN_HEIGHT = Dimensions.get('window').height;

export default class LoginScreen extends Component {

    static navigationOptions = {
        header:  null
    }

    componentWillMount () {
        this.loginHeight = new Animated.Value(150);
        this.keyBoardWillShowListener = Keyboard.addListener('keyboardWillShow',
            this.keyBoardWillShow);
        this.keyBoardWillHideListener = Keyboard.addListener('keyboardWillHide',
            this.keyBoardWillHide);
        this.keyBoardDidShowListener = Keyboard.addListener('keyboardDidShow',
            this.keyBoardWillShow);
        this.keyBoardDidHideListener = Keyboard.addListener('keyboardDidHide',
            this.keyBoardWillHide);
        this.keyboardHeight = new Animated.Value(0);
        this.forwardArrowOpacity = new Animated.Value(0);
        this.borderBottomWidth = new Animated.Value(0);
        this.backgroundImageOpacity = new Animated.Value(0);
    }

    keyBoardWillShow = (event) =>{
        if(Platform.OS == 'android') {
            duration = 100;
            endCoordinatsHeight = SCREEN_HEIGHT - 450;
        } else {
            duration = event.duration;
            endCoordinatsHeight = event.endCoordinats.height;
        }
        Animated.parallel([
            Animated.timing(this.keyboardHeight, {
                duration: duration + 100,
                toValue: endCoordinatsHeight + 10
            }),
            Animated.timing(this.forwardArrowOpacity, {
                duration: duration,
                toValue: 1
            }),
            Animated.timing(this.borderBottomWidth, {
                duration: duration,
                toValue: 1
            })
        ]).start();
    };

    keyBoardWillHide = (event) => {
        if(Platform.OS == 'android') {
            duration = 100;
        } else{
            duration = event.duration;
        }
        Animated.parallel([
            Animated.timing(this.keyboardHeight, {
                duration: duration + 100,
                toValue: 0
            }),
            Animated.timing(this.forwardArrowOpacity, {
                duration: duration,
                toValue: 0
            }),
            Animated.timing(this.borderBottomWidth, {
                duration: duration,
                toValue: 0
            })
        ]).start();
    };

    
    increaseHeight = () =>{
        Animated.timing(this.loginHeight, {
            toValue: SCREEN_HEIGHT ,
            duration: 500
        }).start(() => {
            this.refs.textInputMobile.focus();
        }

        );
    };

    decreaseHeight = () =>{
        Keyboard.dismiss();
        Animated.timing(this.loginHeight, {
            toValue: 150 ,
            duration: 500
        }).start();
    };

    navigateToHome = () => {
        this.props.navigation.navigate('Main');
    };

    render() {

        const headerTextOpacity = this.loginHeight.interpolate({
            inputRange: [150 , SCREEN_HEIGHT],
            outputRange: [1, 0]
        });

        const marginTop = this.loginHeight.interpolate({
            inputRange: [150 , SCREEN_HEIGHT],
            outputRange: [25, 100]
        });

        const headerBackArrowOpacity = this.loginHeight.interpolate({
            inputRange: [150 , SCREEN_HEIGHT],
            outputRange: [0, 1]
        });

        const titleTextBottom = this.loginHeight.interpolate({
            inputRange: [150 , SCREEN_HEIGHT],
            outputRange: [100, 100]
        });

        const titleTextLeft = this.loginHeight.interpolate({
            inputRange: [150 , 400, SCREEN_HEIGHT],
            outputRange: [0, 0, 50]
        });

        const titleTextOpacity = this.loginHeight.interpolate({
            inputRange: [150 , SCREEN_HEIGHT],
            outputRange: [0, 1]
        });

        return(
            <View style={{flex: 1}} >
            <Animated.View style={{ 
                position: 'absolute',
                height:60,
                width:60,
                top:60,
                left:25,
                zIndex:100,
                opacity: headerBackArrowOpacity
            }}>
                <TouchableOpacity
                    onPress= {() => this.decreaseHeight()} >
                        <Icon name="md-arrow-back" style={{color: 'black'}} />
                </TouchableOpacity>
            </Animated.View>
            <Animated.View 
                style={{ 
                    position: 'absolute',
                    height:60,
                    width:60,
                    right:10,
                    bottom:this.keyboardHeight,
                    zIndex: 100,
                    opacity: this.forwardArrowOpacity,
                    backgroundColor: '#54575e',
                    alignItems:'center',
                    justifyContent:'center',
                    borderRadius: 30
            }}>
                <TouchableOpacity
                    onPress= {() => this.navigateToHome()} >
                        <Icon name="md-arrow-forward" style={{color: 'white'}} />
                </TouchableOpacity>        
            </Animated.View>
                <ImageBackground
                    source={require('../../assets/images/login_bg.png')} 
                    style={{flex: 1}}  
                >
                <Animated.View style={{flex: 1, justifyContent: 'center', alignItems: 'center', opacity: headerTextOpacity}}>
                    <Animatable.View 
                        animation="zoomIn"
                        iterationCount={1}
                        style={{ 
                        backgroundColor: 'white', 
                        height: 100, width: 100, 
                        justifyContent: 'center', 
                        alignItems: 'center',
                        elevation : 15
                        }} >
                        <Text style={{ 
                            fontSize: 25, 
                            fontWeight: 'bold',
                            color: 'black'
                            }} > 
                            Bikie 
                        </Text>
                    </Animatable.View>
                </Animated.View>
                <Animatable.View animation="slideInUp" iterationCount={1} >
                    <Animated.View style={{ height: this.loginHeight, backgroundColor: 'white' }}>
                        <Animated.View style={{  alignItems: 'flex-start', marginTop: marginTop , paddingHorizontal: 25, opacity: headerTextOpacity }}>
                            <Text style={{ fontSize: 25, opacity: 1 }} >
                                Get moving with Bikie
                            </Text>
                        </Animated.View>
                        <TouchableOpacity onPress= {() => this.increaseHeight() } >
                            <Animated.View style={{ marginTop: marginTop, paddingHorizontal: 25, flexDirection: 'row' }} >
                                <Animated.Text 
                                    style={{ 
                                        fontSize: 24,
                                        color:'grey',
                                        position:'absolute',
                                        bottom:titleTextBottom,
                                        left: titleTextLeft,                                        
                                        opacity: titleTextOpacity
                                    }} >
                                    Enter your mobile number
                                </Animated.Text>
                                <Image 
                                    source={require('../../assets/images/india.png')} 
                                    style={{
                                    height: 24,
                                    width: 24,
                                    resizeMode: 'contain'
                                    }} />
                                <Animated.View pointerEvents="none"
                                    style= {{   
                                        flexDirection: 'row', 
                                        flex: 1, 
                                        borderBottomWidth: this.borderBottomWidth 
                                    }}>
                                    <Text style= {{ fontSize: 20, paddingHorizontal: 10 }}> 
                                    +91
                                    </Text>
                                    <TextInput 
                                        keyboardType="numeric"
                                        ref="textInputMobile"
                                        style={{ flex: 1, fontSize: 20 }} 
                                        placeholder="Enter your mobile number" 
                                        underlineColorAndroid="transparent"/>
                                </Animated.View>
                            </Animated.View>
                        </TouchableOpacity>
                    </Animated.View>
                </Animatable.View>
                </ImageBackground>    
            </View>
        )
    }
}

const styles = StyleSheet.create({
    container: {
      flex: 1,
    },
  });